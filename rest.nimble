# Package

version       = "0.1.0"
author        = "shdh"
description   = "Rest template"
license       = "MIT"
srcDir        = "src"
bin           = @["rest"]



# Dependencies

requires "nim >= 1.0.4", "jester", "jwt"

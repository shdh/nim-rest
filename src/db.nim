import os
import asyncdispatch
import pg2

let
  host: string = os.getEnv("PG_HOST", "localhost")
  user: string = os.getEnv("PG_USER", "test")
  password: string = os.getEnv("PG_PASS", "pass")
  dbname: string = os.getEnv("PG_DB", "test")
  # poolsize: int = os.getEnv("PG_POOLSIZE", 1)

let db = pg2.open(host, user, password, dbname)

proc test_connection*(): string =
  # let x = waitFor db.rows(sql"SELECT 1", @[])
  for row in waitFor db.rows(sql"SELECT 1", @[]):
    echo row
    return row[0]
